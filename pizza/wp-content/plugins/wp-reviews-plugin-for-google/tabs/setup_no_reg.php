<?php include( plugin_dir_path(__FILE__ ) . "setup_no_reg_header.php" ); ?>

<h1><?php  echo TrustindexPlugin::___('Display your <strong>%s reviews</strong> with our <strong>FREE Widgets</strong>!', [ 'Google' ]); ?></h1>
<?php if(!$trustindex_pm_google->is_noreg_linked()): ?>

<p><?php  echo TrustindexPlugin::___('See an example below:'); ?></p>
	<?php echo $trustindex_pm_google->get_trustindex_widget('2d9bf9019f8d93ad1430e9135'); ?>

<?php endif; ?>

<h2><?php  echo TrustindexPlugin::___('Follow these 3 steps to embed %s in 2 minutes:', [ 'Google Reviews' ]); ?></h2>
<ul class="numbered">
<li>
<h3><?php  echo TrustindexPlugin::___("Type your business/company's Place ID or your Google Search/Google Maps URL:"); ?></h3>
		<?php if($trustindex_pm_google->is_noreg_linked()): ?>

<p>
				<?php echo TrustindexPlugin::___("Your %s is connected.", [ TrustindexPlugin::___('Google Reviews') ]); ?><br />

				<?php $page_details = get_option($trustindex_pm_google->get_option_name('page-details')); ?>

- <?php  echo TrustindexPlugin::___("Name") ; ?>: <strong style="font-size: 115%"><?php  echo $page_details['name']; ?></strong><br />
				<?php if ($page_details['address']): ?>

- <?php  echo TrustindexPlugin::___("Address"); ?>: <?php  echo $page_details['address']; ?><br />
				<?php endif; ?>

</p>
<form method="post" action="">
<input type="hidden" name="command" value="delete-page" />
				<?php wp_nonce_field( 'delete-noreg_'.$trustindex_pm_google->get_plugin_slug(), '_wpnonce_delete' ); ?>

<button class="btn btn-delete" type="submit"><?php  echo TrustindexPlugin::___("Disconnect"); ?></button>
</form>
		<?php else: ?>

<form method="post" action="" data-platform="google" id="submit-form">
<input type="hidden" name="command" value="save-page" />
				<?php wp_nonce_field( 'save-noreg_'.$trustindex_pm_google->get_plugin_slug(), '_wpnonce_save' ); ?>

<input type="hidden"
name="page_details"
class="form-control"
required="required"
id="ti-noreg-page_details"
value=""
/>
<div class="autocomplete" style="width: 600px; float: left;">
<input class="form-control"
placeholder="<?php  echo TrustindexPlugin::___("e.g.:") . ' ' . $example; ?>"
id="page-link"
type="text" required="required"
/>
<img class="loading" src="<?php  echo admin_url('images/loading.gif'); ?>" />
<div class="results"
data-errortext="<?php  echo TrustindexPlugin::___("Please add your Place ID again: this is not a valid %s page.", [ "Google" ]); ?>"
data-errortext-search="<?php  echo TrustindexPlugin::___("Please give us that URL where you can see your business profile in Google Search or in Google Maps! For example: <a href='%s' target='_blank'>illustration</a>, <a href='%s' target='_blank'>illustration</a>", [ 'https://admin.trustindex.io/' . 'images/trustindex-google-search-1.jpg', 'https://admin.trustindex.io/' . 'images/trustindex-google-search-2.jpg' ]); ?>"
></div>
</div>
<button class="btn btn-primary btn-check"><?php  echo TrustindexPlugin::___("Check") ;?></button>
<div style="display: none;" class="div-selected-page-info">
<p><?php  echo TrustindexPlugin::___('Selected page:'); ?></p>
<strong id="label-noreg-page_name"></strong><br />
<span id="label-noreg-address"></span><br />
<span id="label-noreg-url"></span>
<button class="btn btn-primary btn-connect" data-loading-text="<?php  echo TrustindexPlugin::___("Loading") ;?>"><?php  echo TrustindexPlugin::___("Connect") ;?></button>
</div>
</form>
		<?php endif; ?>

</li>
	<?php include( plugin_dir_path(__FILE__ ) . "setup_no_reg_widget.php" ); ?>

	<?php include( plugin_dir_path(__FILE__ ) . "setup_no_reg_footer.php" ); ?>

</ul>