��    �      T    �     �     �  #   �       K   5  )   �     �     �  (   �  &     f   +     �     �     �     �     �     �     �       	     
   #     .  
   :  	   E  
   O     Z     f     �     �     �     �     �     �     �          $     :  -   B     p  *   �     �  �   �  B   f  '   �  2   �          $  
   1     <  
   A  #   L  P   p     �     �  t   �     m     �     �     �     �  w   �     =     Q     Z  .   g     �     �     �      �     �     	       s   -     �     �     �     �     �     �  !   �  _        ~     �     �  
   �     �     �  �   �  Q   �  5   �  '   '  ;   O  *   �     �     �     �     �     �     �     �  
                 /     =     T     c  [   l  <   �  7     e   =  �   �  '   q      �      �      �      �      �      �   e   �      =!     V!     q!     z!     �!     �!     �!     �!     �!     �!     �!     "     "     "     %"     <"     R"     l"     ~"  *   �"  	   �"  
   �"     �"     �"  !   #  
   )#     4#     J#     _#     w#     |#  0   �#     �#  C   �#     $     +$     7$     H$     X$  *   k$  -   �$     �$  L   �$  L   !%  :   n%  ;   �%  B   �%  a   (&     �&     �&  H   �&     �&  �   '  =   �'     �'  Z   (  V   f(  (   �(  �   �(  ~   �)  :   *  �   J*  2   +     4+  0   N+  '   +  /   �+  4   �+  ^   ,     k,     �,     �,  E   �,     �,  C   �,  r   B-     �-     �-     �-  ,   �-     �-  5   .  ,   G.  ~   t.  B   �.  #   6/  8   Z/  A   �/  >   �/  �   0  
   �0  '   1     -1  ;   41  ,   p1     �1  +   �1     �1     �1     �1      2     2     .2     :2     Q2  5   h2  5   �2     �2  )   �2     3  9   $3  !   ^3     �3  7   �3  )   �3     �3  P   
4  G   [4  g   �4     5  �   (5  V   6  D   _6  O   �6  .   �6     #7     87  
   L7     W7  ;   m7  p   �7  !   8  ,   <8  �   i8  $   +9     P9  (   Y9  0   �9     �9  �   �9  /   �:     �:     �:  O   �:  +   <;      h;  +   �;  =   �;  ,   �;  -    <  0   N<  �   <  &   R=     y=     �=     �=     �=     �=  %   �=  �   �=  ,   ~>     �>  7   �>     ?  +   ?  K   7?     �?  �   �@  }   A  P   �A  �   �A  <   nB     �B     �B     �B     �B  .   
C     9C     SC     qC     �C  *   �C  �  �C  *   iE     �E     �E  �   �E  g   �F  n   �F  �   cG    �G  k   �H     kI     }I  
   �I  8   �I  
   �I  1   �I  �   J  "   �J  H   K     OK     ^K  -   qK     �K  !   �K      �K     �K     �K     L     'L     DL     SL      cL  &   �L  F   �L  2   �L  3   %M  2   YM     �M     �M  ,   �M  -   �M  4   N     TN     qN  ,   �N  +   �N     �N     �N  <   O  *   >O  V   iO     �O  
   �O     �O  .   �O     )P  +   =P  6   iP     �P  �   �P  w   YQ  O   �Q  G   !R  Y   iR  �   �R  
   kS  D   vS  `   �S     T  C  8T  �   |U     V  t    V  s   �V  8   	W    BW  �   KX  T   Y  A  iY  9   �Z  +   �Z  M   [  )   _[  R   �[  Y   �[  �   6\     �\     �\  3   �\  i   #]  *   �]  c   �]  �   ^     �^  
   �^     �^  �   %   1   R   *      �   �   z   ?   �   �   `   B   u       E   x      �             m       r       ^   }   �   "          (          O   T       �       �   .           |   �       �   �       �      a          L           5   �               �   �   �      4       �   =   �       +   �   g   �   2   N   n                    [           '   �   	   k               �      v   c   p           �   �       /           �               �           F   M      h   �       
   3   �   �      G   ;   �                   $   j              �   b   0   I   o   ~   �      �   i   �   �   �   l      >                 �       Z   !   �       {   8   :   6      w           t       P   �          �   d   �           �   �   C   Q       �   U       �   y      ]   �   �   <   )   �       �   �   9   e   J   �   �   ,       \   S       Y       �   D   �   &      H       �   _   �   @           W      f   V      �   �       X   A       �   �       �   �   �   K           �   s   �      7      �   �   �           �   �   #             q   �   -   (* with onboarding discount) -- Thanks, Gabor M. 1. Reviews from ALL these platforms 2. Unlock new, exclusive styles 20,000+ WordPress websites use Trustindex to embed reviews fast and easily. 3. Hide Negative Reviews - THIS IS GREAT! 4. New Review Notification 5. Get More Reviews! 6. Automatically update with NEW reviews 7. Display UNLIMITED number of reviews Add more reviews to your widget from %s, etc. to enjoy more trust, and to keep customers on your site. Address Airbnb profile page Amazon page Automatic plugin update Automatic review update Badge Bookatable profile page Booking.com page Button I. Button II. Button III. Button IV. Button V. Button VI. Button VII. Button VII. - with dropdown Button VII. - with popup CONNECT ACCOUNT Capterra profile page Check City ( e.g.: New York) Click here to rate us! Company badge I. Company badge I. - with popup Configure your Widget Connect Connect your %s platform to download reviews. Connect your %s to our Plugin Connect your Trustindex page to our plugin Copy to clipboard Could not download the template for the widget.<br />Please reload the page.<br />If the problem persists, please write an email to support@trustindex.io. Could you please help us and give it a 5-star rating on WordPress? Create a Free Account for More Features Create a Free Trustindex Account for More Features Create a new Trustindex account Create more! DAILY DEAL Date Disconnect Display unlimited number of reviews Display your <strong>%s reviews</strong> with our <strong>FREE Widgets</strong>! Do not remind me again Do not skip the previous steps! Don't want to waste your time constantly updating? Trustindex paid packages solve this for you from $3.75* per month Download new reviews Enable Enable animation Exclusive styles and designs Facebook Page Fast onboarding discount: save 30%% on your annual fee. See details after <a href='%s' target='_blank'>registration</a> Filter your ratings Floating Floating II. Follow these 3 steps to embed %s in 2 minutes: Foursquare profile page Free Widget Configurator Full sidebar I. Full sidebar I. - without header Full sidebar II. Full sidebar III. Get more Features Go to <a href='%s'>plugin setup page</a> to complete the one-step setup guide and enjoy the full functionalization! Go to Trustindex's admin! Google Reviews Grid HTML badge I. HTML badge II. HTML badge III. Have you forgotten your password? Hello, I am happy to see that you've been using our <strong>%s</strong> plugin for a while now! Hide negative reviews Hide rating text Hide reviews without comments Horizontal Hotels.com profile page How to use our plugin If you have any questions or problem, please send an email to support@trustindex.io included the information below and our colleagues will answer you in 48 hours! If you notice an incorrect translation in the plugin text, please report it here: Increase SEO, trust and sales using customer reviews. Insert this shortcode into your website Insert your widget into your wordpress site using shortcode Let's go, create amazing widgets for free! Link List I. Loading Log In Manage your Trustindex account Mansonry Grid Maybe later My Reviews Name New reviews loaded! No results. %s cannot find your business by the terms you gave. Do not panic! There is an unique business search function in Trustindex, you only need to register for free and it will help you to find your business/store. Check out the next tab, called 'More Features'! Opentable profile page Other settings Password Pick from 13 widget layouts and 9 predesigned styles to match the branding of your website. Please add your Place ID again: this is not a valid %s page. Please add your URL again: this is not a valid %s page. Please fill out <strong>all the required fields</strong> in the <a href='%s'>widget settings</a> page Please give us that URL where you can see your business profile in Google Search or in Google Maps! For example: <a href='%s' target='_blank'>illustration</a>, <a href='%s' target='_blank'>illustration</a> Please help us by reviewing our Plugin. Popup Preview Rate Us Rate our plugin Rating Re-create plugin Re-create the database tables of the plugin.<br />Please note: this removes all settings and reviews. Review invitation system Review notification emails Reviewer Reviews Reviews from 17+ platforms Search See an example below: Select date format Select language Select your style Select your template Selected page: Settings Show all Show navigation arrows Show reviewers' photo Show verified review icon Sidebar slider I. Sidebar slider II. Skyrocket Your Sales with Customer Reviews Slider I. Slider II. Slider III. - with badge Slider III. - with badge II. Slider III. - with company header Slider IV. Something went wrong. Sure, you deserve it Szallas.hu profile page Text Thank you in advance! The widget will be pinned to bottom of the page. Thumbtack profile page Too short! Please enter your business' name and city, if applicable Top Features Translation Tripadvisor page Troubleshooting Trustindex account Trustindex account successfully connected! Trustindex account successfully disconnected! Trustpilot page Turn on email alert to ALL new reviews, so that you can manage them quickly. Type your business/company's Place ID or your Google Search/Google Maps URL: Type your business/company's URL and select from the list: Type your business/company's name and select from the list: Type your business/company's name or URL and select from the list: Use our Review Invitation System to collect hundreds of new reviews. Become impossible to resist! Vertical Want to get more customers? We will redirect you to the admin panel automatically in some seconds... Widget settings Wordpress cannot update reviews, but Trustindex can! As soon as you get a new review, Trustindex Business can automatically add it to your website. Customers love fresh reviews! Wrong e-mail or password! <a href='%s'>You can try again!</a> Yelp Business page You can connect your %s with your Trustindex account, and can display your widgets easier. You can mix your reviews from different platforms and display them in 1 review widget. You can only list 10 reviews without it. You can test Trustindex with 10 reviews in the free version. Upgrade to Business to display ALL the reviews received. Be the undisputed customer choice in your industry! You do not have reviews with the current filters. <br />Change your filters if you would like to display reviews on your page! You had no reviews at the time of last review downloading. You have connected your Trustindex account, so you can find premium functionality under the "%s" tab. You no longer need this tab unless you choose the limited but forever free mode. You have got %d widgets saved in Trustindex admin. You have no widget saved! You have not set up your Trustindex account yet! You have to connect your business (%s)! You must provide a password and a valid e-mail! You must provide a password and a valid e-mail, url! You should enable it, to get new features and fixes automatically, right after they published! Your %s is connected. Your package: Your saved widgets Your shortcode is deficient: Trustindex Widget ID is empty! Example:  Your subscription ID: Your widgets will not display fully after the trial period is over! Your widgets will not display, because the trial period is over! You can subscribe or switch back the the "%s" tab e.g.: free only (* مع خصم على متن الطائرة) - شكرًا ، جابور م. 1. مراجعات من جميع هذه المنصات 2. فتح أنماط جديدة وحصرية أكثر من 20000 موقع على WordPress تستخدم Trustindex لتضمين المراجعات بسرعة وسهولة. 3. إخفاء التعليقات السلبية - هذا رائع! 4. إشعار مراجعة جديد 5. احصل على المزيد من التعليقات! 6. التحديث تلقائيًا مع مراجعات جديدة 7. عرض عدد غير محدود من الاستعراضات أضف المزيد من المراجعات إلى عنصر واجهة المستخدم الخاص بك من%s ، وما إلى ذلك ، للتمتع بمزيد من الثقة ، ولإبقاء العملاء على موقعك. عنوان Airbnb صفحة الملف الشخصي Amazon تحديث البرنامج المساعد التلقائي تحديث المراجعة التلقائي شارة Bookatable صفحة الملف الشخصي صفحة Booking.com الزر I. الزر II. الزر الثالث. الزر الرابع. الزر V. الزر السادس. الزر السابع. الزر السابع. - مع قائمة منسدلة الزر السابع. - مع نافذة منبثقة ربط الحساب Capterra صفحة الملف الشخصي التحقق من مدينة (على سبيل المثال: نيويورك) انقر هنا لتقييمنا! شارة الشركة I. شارة الشركة I. - مع نافذة منبثقة تكوين القطعة الخاصة بك الاتصال قم بتوصيل النظام الأساسي%s لتنزيل المراجعات. قم بتوصيل%s بالبرنامج الإضافي الخاص بنا قم بتوصيل صفحة Trustindex الخاصة بك بالمكون الإضافي الخاص بنا نسخ إلى الحافظة تعذر تنزيل النموذج للأداة. <br />الرجاء إعادة تحميل الصفحة. <br />إذا استمرت المشكلة ، يرجى إرسال بريد إلكتروني إلى support@trustindex.io. هل يمكنك مساعدتنا ومنحها تصنيف 5 نجوم على WordPress؟ قم بإنشاء حساب مجاني لمزيد من الميزات قم بإنشاء حساب Trustindex مجاني لمزيد من الميزات قم بإنشاء حساب Trustindex جديد خلق المزيد! صفقة يومية تاريخ قطع الاتصال عرض عدد غير محدود من الاستعراضات اعرض <strong>%s تقييماتك</ strong>باستخدام <strong>أدواتنا المجانية</strong>! لا تذكرني مرة أخرى لا تخطي الخطوات السابقة! لا تريد أن تضيع وقتك في التحديث باستمرار؟ تحل حزم Trustindex المدفوعة هذا الأمر من 3.75 دولارًا أمريكيًا في الشهر تنزيل مراجعات جديدة ممكن تفعيل الرسوم المتحركة الأنماط والتصاميم الحصرية صفحة الفيسبوك خصم سريع على الصعود على متن الطائرة: وفر 30٪ من الرسوم السنوية. راجع التفاصيل بعد <a href='%s' target='_blank'>التسجيل</a> تصفية التصنيفات الخاصة بك يطفو على السطح العائمة II. اتبع هذه الخطوات الثلاث لتضمين%s في دقيقتين: Foursquare صفحة الملف الشخصي مكون مجاني للقطعة الشريط الجانبي الكامل I. الشريط الجانبي الكامل I. - بدون رأس الشريط الجانبي الكامل II. الشريط الجانبي الكامل III. احصل على المزيد من الميزات انتقل إلى <a href='%s'>صفحة إعداد المكون الإضافي</a>لإكمال دليل الإعداد المكون من خطوة واحدة والاستمتاع بالوظائف الكاملة! انتقل إلى مشرف Trustindex! Google المراجعات جريد شارة HTML I. شارة HTML II. شارة HTML III. هل نسيت كلمة المرور؟ مرحبًا ، يسعدني أن أرى أنك تستخدم المكون الإضافي <strong>%s</strong>منذ فترة الآن! إخفاء المراجعات السلبية إخفاء نص التقييم إخفاء الاستعراضات دون تعليقات عرضي صفحة الملف الشخصي Hotels.com كيفية استخدام البرنامج المساعد الخاص بنا إذا كانت لديك أي أسئلة أو مشكلة ، فيرجى إرسال بريد إلكتروني إلى support@trustindex.io متضمنًا المعلومات أدناه وسيقوم زملاؤنا بالرد عليك في غضون 48 ساعة! إذا لاحظت وجود ترجمة غير صحيحة في نص المكون الإضافي ، فيرجى الإبلاغ عنها هنا: زيادة تحسين محركات البحث والثقة والمبيعات باستخدام تقييمات العملاء. أدخل هذا الرمز القصير في موقع الويب الخاص بك أدخل عنصر واجهة المستخدم الخاص بك في موقع WordPress الخاص بك باستخدام الرمز القصير لنبدأ ، وننشئ أدوات مدهشة مجانًا! حلقة الوصل القائمة الأولى. جار التحميل تسجيل الدخول إدارة حساب Trustindex الخاص بك شبكة مانسونري ربما في وقت لاحق تعليقاتي اسم تم تحميل مراجعات جديدة! لا نتائج. لم يتمكن%s من العثور على عملك بالشروط التي قدمتها. لا تصب بالذعر! هناك وظيفة فريدة للبحث عن الأعمال في Trustindex ، ما عليك سوى التسجيل مجانًا وستساعدك في العثور على عملك / متجرك. تحقق من علامة التبويب التالية ، المسماة "المزيد من الميزات"! Opentable صفحة الملف الشخصي اعدادات اخرى كلمه السر اختر من بين 13 تخطيطًا لعناصر واجهة المستخدم و 9 أنماط مصممة مسبقًا لمطابقة العلامة التجارية لموقع الويب الخاص بك. الرجاء إضافة معرّف المكان مرة أخرى: هذه ليست صفحة%s صالحة. الرجاء إضافة عنوان URL الخاص بك مرة أخرى: هذه ليست صفحة%s صالحة. يرجى ملء <strong>جميع الحقول المطلوبة</strong>في صفحة <a href='%s'>إعدادات الأداة</a> يرجى تزويدنا بعنوان URL الذي يمكنك من خلاله مشاهدة ملفك التجاري في Google Search أو في خرائط Google! على سبيل المثال: <a href='%s' target='_blank'>توضيح</a>، <a href='%s' target='_blank'>توضيح</a> الرجاء مساعدتنا من خلال مراجعة البرنامج المساعد الخاص بنا. يظهر فجأة معاينة قيمنا قيم البرنامج المساعد الخاص بنا تقييم أعد إنشاء البرنامج المساعد أعد إنشاء جداول قاعدة البيانات الخاصة بالمكون الإضافي. <br />يُرجى ملاحظة: يؤدي هذا إلى إزالة جميع الإعدادات والمراجعات. مراجعة نظام الدعوة مراجعة رسائل البريد الإلكتروني الإخطار المراجع المراجعات مراجعات من أكثر من 17 منصة بحث انظر المثال أدناه: حدد تنسيق التاريخ اختار اللغة اختر طريقتك حدد قالبك الصفحة المحددة: إعدادات عرض الكل إظهار أسهم التنقل إظهار صورة المراجعين إظهار رمز المراجعة التي تم التحقق منها شريط تمرير الشريط الجانبي I. شريط تمرير الشريط الجانبي II. صعد مبيعاتك مع آراء العملاء شريط التمرير I. المنزلق الثاني. المنزلق الثالث. - مع شارة المنزلق الثالث. - بشارة II. المنزلق الثالث. - برأس الشركة المنزلق الرابع. هناك خطأ ما. بالتأكيد ، أنت تستحق ذلك صفحة الملف الشخصي Szallas.hu نص شكرا لكم مقدما! سيتم تثبيت الأداة في أسفل الصفحة. Thumbtack صفحة الملف الشخصي قصير جدا! الرجاء إدخال اسم ومدينة عملك ، إن أمكن أهم الميزات ترجمة Tripadvisor الصفحة استكشاف الأخطاء وإصلاحها حساب Trustindex تم ربط حساب Trustindex بنجاح! تم قطع اتصال حساب Trustindex بنجاح! Trustpilot الصفحة قم بتشغيل تنبيه البريد الإلكتروني لجميع المراجعات الجديدة ، حتى تتمكن من إدارتها بسرعة. اكتب معرّف مكان شركتك / شركتك أو عنوان URL الخاص بخرائط Google Search / Google اكتب عنوان URL لشركتك / شركتك وحدد من القائمة: اكتب اسم شركتك / شركتك واختر من القائمة: اكتب اسم شركتك / شركتك أو عنوان URL وحدد من القائمة: استخدم نظام دعوة المراجعة الخاص بنا لجمع مئات المراجعات الجديدة. أصبح من المستحيل المقاومة! عمودي هل تريد الحصول على المزيد من العملاء؟ سنعيد توجيهك إلى لوحة الإدارة تلقائيًا في بضع ثوان ... إعدادات القطعة لا يمكن لـ Wordpress تحديث المراجعات ، لكن Trustindex يمكنها ذلك! بمجرد حصولك على مراجعة جديدة ، يمكن لـ Trustindex Business إضافتها تلقائيًا إلى موقع الويب الخاص بك. يحب العملاء التقييمات الجديدة! البريد الإلكتروني أو كلمة المرور خاطئة! <a href='%s'>يمكنك المحاولة مرة أخرى!</a> Yelp صفحة الأعمال يمكنك توصيل%s بحساب Trustindex الخاص بك ، ويمكنك عرض أدواتك بشكل أسهل. يمكنك مزج تعليقاتك من منصات مختلفة وعرضها في أداة مراجعة واحدة. يمكنك فقط سرد 10 تقييمات بدونها. يمكنك اختبار Trustindex من خلال 10 مراجعات في الإصدار المجاني. قم بالترقية إلى Business لعرض جميع المراجعات المستلمة. كن خيار العميل بلا منازع في مجال عملك! ليس لديك مراجعات مع عوامل التصفية الحالية. <br />قم بتغيير عوامل التصفية إذا كنت ترغب في عرض التعليقات على صفحتك! لم يكن لديك أي مراجعات في وقت آخر مراجعة تنزيل. لقد قمت بتوصيل حساب Trustindex الخاص بك ، لذا يمكنك العثور على وظائف متميزة ضمن علامة التبويب "%s". لم تعد بحاجة إلى علامة التبويب هذه إلا إذا اخترت الوضع المحدود ولكنه مجاني إلى الأبد. لديك%d أدوات محفوظة في Trustindex admin. ليس لديك أي أداة محفوظة! لم تقم بإعداد حساب Trustindex الخاص بك حتى الآن! يجب عليك توصيل عملك (%s)! يجب عليك تقديم كلمة مرور وبريد إلكتروني صالح! يجب عليك تقديم كلمة مرور وبريد إلكتروني صالح ، url! يجب عليك تمكينه للحصول على ميزات وإصلاحات جديدة تلقائيًا ، مباشرة بعد نشرها! %s الخاص بك متصل. مجموعتك: الحاجيات المحفوظة الخاصة بك الرمز المختصر الخاص بك غير صحيح: معرف Trustindex Widget فارغ! مثال: معرف الاشتراك الخاص بك: لن يتم عرض أدواتك بالكامل بعد انتهاء الفترة التجريبية! لن يتم عرض أدواتك ، لأن الفترة التجريبية قد انتهت! يمكنك الاشتراك أو إعادة علامة التبويب "%s" على سبيل المثال: مجانا فقط 